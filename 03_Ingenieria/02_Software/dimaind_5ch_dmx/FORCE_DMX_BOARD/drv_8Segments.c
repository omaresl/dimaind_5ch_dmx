/*
 * drv_8Segments.c
 *
 * Created: 22/11/2019 07:56:02 a.m.
 *  Author: uidj2522
 */ 

 #include "drv_8Segment.h"
 #include "util/delay.h"

 typedef enum
 {
	DIGIT_1_STATE,
	DIGIT_2_STATE,
	DIGIT_3_STATE,
	N_DIGITS
 }T_DIGIT_STATE;

 //List of digit value for each symbol
 unsigned char raub_DigitTable[16u] =
 {
	0b00000011,//0
	0b10011111,//1
	0b00100101,//2
	0b00001101,//3
	0b10011001,//4
	0b01001001,//5
	0b01000001,//6
	0b00011111,//7
	0b00000001,//8
	0b00011001,//9
	0b00010001,//A
	0b11000001,//B
	0b01100011,//C
	0b10000101,//D
	0b01100001,//E
	0b01110001 //F
 };

 /* Task execution flag */
 unsigned char rub_8SegTaskFlag = 0x00u;

 T_DIGIT_STATE re_DigitState = DIGIT_1_STATE;
 unsigned char raub_DigitData[N_DIGITS] = {0,0,0};

 static void drv_8Segment_SetDigit(unsigned char lub_Digit);

 /************************************************************************/
 /* Name: drv_8Segment_Init                                              */
 /* Description: Initialize 8Segment Lines			    				 */
 /* Parameters: None                                                     */
 /* Return: None                                                         */
 /************************************************************************/
 void drv_8Segment_Init(void)
 {
	DRV_8SEGMENT_ALL_DIGITS_OFF;
	re_DigitState = DIGIT_1_STATE;
 }

 /************************************************************************/
 /* Name: drv_8Segment_SetDigit                                          */
 /* Description: Set in Digit buffer the selected number   				 */
 /* Parameters: None                                                     */
 /* Return: None                                                         */
 /************************************************************************/
 static void drv_8Segment_SetDigit(unsigned char lub_Digit)
 {
 #define N_SERIAL_BITS	8u	//Number of digits to be transmitted

	//Set in high CLK line
	DRV_GPIO_SET_SEG_CLK;
	for (unsigned char lub_i = 0; lub_i < N_SERIAL_BITS; lub_i++)
	{
		if(0x00 != (lub_Digit & (unsigned char)(0x01u << lub_i ))) //Evaluate bits
		{
			//Bit set
			DRV_GPIO_SET_SEG_IN;
		}
		else
		{
			//Bit clear
			DRV_GPIO_CLEAR_SEG_IN;
		}

		//Clock Sequence
		DRV_GPIO_CLEAR_SEG_CLK;
		DRV_GPIO_SET_SEG_CLK;
	}
 }

  /************************************************************************/
 /* Name: drv_8Segment_Task												 */
 /* Description: Main Task of driver   									 */
 /* Parameters: None                                                     */
 /* Return: None                                                         */
 /************************************************************************/
 void drv_8Segment_Task(void)
 {
 static unsigned char rub_TickCounter = 0u;

	if (0x00 != rub_8SegTaskFlag)
	{
		//Consume flag
		rub_8SegTaskFlag = 0u;
		if(rub_TickCounter >= DRV_8SEGMENT_TIMER_MULTIPLIER)
		{
			rub_TickCounter = 0u;
			//Off all digits
			DRV_8SEGMENT_ALL_DIGITS_OFF;
			switch(re_DigitState)
			{
			default:
			case DIGIT_1_STATE:
			{
				drv_8Segment_SetDigit(raub_DigitTable[raub_DigitData[DIGIT_1_STATE]]);
				DRV_8SEGMENT_DIGIT1_ON;
				re_DigitState = DIGIT_2_STATE;
			}break;
			case DIGIT_2_STATE:
			{
				drv_8Segment_SetDigit(raub_DigitTable[raub_DigitData[DIGIT_2_STATE]]);
				DRV_8SEGMENT_DIGIT2_ON;
				re_DigitState = DIGIT_3_STATE;
			}break;
			case DIGIT_3_STATE:
			{
				drv_8Segment_SetDigit(raub_DigitTable[raub_DigitData[DIGIT_3_STATE]]);
				DRV_8SEGMENT_DIGIT3_ON;
				re_DigitState = DIGIT_1_STATE;
			}break;
			}
		}
		else
		{
			rub_TickCounter++;
		}
	} 
	else
	{
		/* Do nothing */
	}
 }

 /************************************************************************/
 /* Name: drv_8Segment_SetDecNumber										 */
 /* Description: Set in number buffer the data to be shown				 */
 /* Parameters: None                                                     */
 /* Return: None                                                         */
 /************************************************************************/
 void drv_8Segment_SetDecNumber(unsigned short luw_Number)
 {
	raub_DigitData[DIGIT_1_STATE] = (unsigned char)(luw_Number / 100u); //Hundreds
	luw_Number -= (unsigned short)(raub_DigitData[DIGIT_1_STATE])*100u;
	raub_DigitData[DIGIT_2_STATE] = (unsigned char)(luw_Number / 10u); //Tens
	luw_Number -= (unsigned short)(raub_DigitData[DIGIT_2_STATE])*10u;
	raub_DigitData[DIGIT_3_STATE] = (unsigned char)(luw_Number); //Units
 }