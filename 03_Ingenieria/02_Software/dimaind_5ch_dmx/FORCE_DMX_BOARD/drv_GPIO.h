/*
 * drv_GPIO.h
 *
 * Created: 17/07/2019 11:33:14 p. m.
 *  Author: OmarSevilla
 */ 


#ifndef DRV_GPIO_H_
#define DRV_GPIO_H_

/* Interfaces */
#include "atmel_start_pins.h"

/* Public Enumeration */

/* Public Definitions */
#define DRV_GPIO_SET_RX_DATA		RXD_DATA_set_level(0x01)
#define DRV_GPIO_SET_TX_DATA		TXD_DATA_set_level(0x01)
#define DRV_GPIO_SET_DE				DE_set_level(0x01)
#define DRV_GPIO_SET_MODE			MODE_set_level(0x01)
#define DRV_GPIO_SET_XLAT			XLAT_set_level(0x01)
#define DRV_GPIO_SET_BLANK			BLANK_set_level(0x01)
#define DRV_GPIO_SET_SIN			SIN_set_level(0x01)
#define DRV_GPIO_SET_SCLK			SCLK_set_level(0x01)
#define DRV_GPIO_SET_GSCLK			GSCLK_set_level(0x01)
#define DRV_GPIO_SET_LED_RED		
#define DRV_GPIO_SET_LED_GREEN		
#define DRV_GPIO_SET_LED_BLUE		
#define DRV_GPIO_SET_DIGIT_1		DIGIT_1_set_level(0x01)
#define DRV_GPIO_SET_DIGIT_2		DIGIT_2_set_level(0x01)
#define DRV_GPIO_SET_DIGIT_3		DIGIT_3_set_level(0x01)
#define DRV_GPIO_SET_SEG_CLK		SEG_CLK_set_level(0x01)
#define DRV_GPIO_SET_SEG_IN			SEG_IN_set_level(0x01)

#define DRV_GPIO_CLEAR_RX_DATA		RXD_DATA_set_level(0x00)
#define DRV_GPIO_CLEAR_TX_DATA		TXD_DATA_set_level(0x00)
#define DRV_GPIO_CLEAR_DE			DE_set_level(0x00)
#define DRV_GPIO_CLEAR_MODE			MODE_set_level(0x00)
#define DRV_GPIO_CLEAR_XLAT			XLAT_set_level(0x00)
#define DRV_GPIO_CLEAR_BLANK		BLANK_set_level(0x00)
#define DRV_GPIO_CLEAR_SIN			SIN_set_level(0x00)
#define DRV_GPIO_CLEAR_SCLK			SCLK_set_level(0x00)
#define DRV_GPIO_CLEAR_GSCLK		GSCLK_set_level(0x00)
#define DRV_GPIO_CLEAR_LED_RED		
#define DRV_GPIO_CLEAR_LED_GREEN	
#define DRV_GPIO_CLEAR_LED_BLUE		
#define DRV_GPIO_CLEAR_DIGIT_1		DIGIT_1_set_level(0x00)
#define DRV_GPIO_CLEAR_DIGIT_2		DIGIT_2_set_level(0x00)
#define DRV_GPIO_CLEAR_DIGIT_3		DIGIT_3_set_level(0x00)
#define DRV_GPIO_CLEAR_SEG_CLK		SEG_CLK_set_level(0x00)
#define DRV_GPIO_CLEAR_SEG_IN		SEG_IN_set_level(0x00)

#define DRV_GPIO_GET_RX_DATA		RXD_DATA_get_level()
#define DRV_GPIO_GET_GSCLK			GSCLK_get_level()
#define DRV_GPIO_GET_BTN_1			BTN_1_get_level()
#define DRV_GPIO_GET_BTN_2			BTN_2_get_level()

#endif /* DRV_GPIO_H_ */