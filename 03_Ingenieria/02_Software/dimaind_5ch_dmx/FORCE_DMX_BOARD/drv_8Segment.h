/*
 * drv_8Segment.h
 *
 * Created: 22/11/2019 07:57:30 a.m.
 *  Author: uidj2522
 */ 


#ifndef DRV_8SEGMENT_H_
#define DRV_8SEGMENT_H_

#include "drv_GPIO.h"

/* Digit Actions */
#define DRV_8SEGMENT_DIGIT1_ON	DRV_GPIO_SET_DIGIT_1
#define DRV_8SEGMENT_DIGIT2_ON	DRV_GPIO_SET_DIGIT_2
#define DRV_8SEGMENT_DIGIT3_ON	DRV_GPIO_SET_DIGIT_3

#define DRV_8SEGMENT_DIGIT1_OFF	DRV_GPIO_CLEAR_DIGIT_1
#define DRV_8SEGMENT_DIGIT2_OFF	DRV_GPIO_CLEAR_DIGIT_2
#define DRV_8SEGMENT_DIGIT3_OFF	DRV_GPIO_CLEAR_DIGIT_3

#define DRV_8SEGMENT_ALL_DIGITS_OFF	do{\
DRV_8SEGMENT_DIGIT1_OFF;\
DRV_8SEGMENT_DIGIT2_OFF;\
DRV_8SEGMENT_DIGIT3_OFF;\
} while (0)

#define DRV_8SEGMENT_TIMER_MULTIPLIER	1u	//10Ticks
/* Public Data */
unsigned char rub_8SegTaskFlag; //Must be used in a Timer task

/* Public Functions */
extern void drv_8Segment_Init(void);
extern void drv_8Segment_Task(void);
extern void drv_8Segment_SetDecNumber(unsigned short lub_Number);


#endif /* DRV_8SEGMENT_H_ */