/*
* app_BtnMngr.c
*
* Created: 22/11/2019 10:48:27 a.m.
*  Author: uidj2522
*/
#include "app_BtnMngr.h"

T_BTN_State re_BtnState[APP_BTNMNGR_N_BUTTONS] = {NOT_PRESSED, NOT_PRESSED};
unsigned short	ruw_BtnDebounceCounter[APP_BTNMNGR_N_BUTTONS];
unsigned char	rub_BtnMngrTaskFlag = false;

static void app_BtnMngr_DebBtn1(void);
static void app_BtnMngr_DebBtn2(void);


/************************************************************************/
/* Name: app_BtnMngr_Init                                               */
/* Description: TBD                       								*/
/* Parameters: None                                                     */
/* Return: None                                                         */
/************************************************************************/
void app_BtnMngr_Init(void)
{
	APP_BTNMNGR_BTN_1_STATE = NOT_PRESSED;
	APP_BTNMNGR_BTN_2_STATE = NOT_PRESSED;

	//Clear counters
	APP_BTNMNGR_BTN_1_DCNTR = 0u;
	APP_BTNMNGR_BTN_2_DCNTR = 0u;
}

/************************************************************************/
/* Name: app_BtnMngr_Task                                               */
/* Description: TBD                     								*/
/* Parameters: None                                                     */
/* Return: None                                                         */
/************************************************************************/
void app_BtnMngr_Task(void)
{
	if(rub_BtnMngrTaskFlag == true)
	{
		rub_BtnMngrTaskFlag = false;
		app_BtnMngr_DebBtn1();
		app_BtnMngr_DebBtn2();
	}else{}
}

/************************************************************************/
/* Name: app_BtnMngr_DebBtn1                                            */
/* Description: TBD                     								*/
/* Parameters: None                                                     */
/* Return: None                                                         */
/************************************************************************/
static void app_BtnMngr_DebBtn1(void)
{
	//Check input
	if(0x00 == APP_BTNMNGR_READ_BTN_1)
	{//Button Pressed
		//Check debounce
		if(APP_BTNMNGR_BTN_1_DCNTR >= APP_BTNMNGR_VERYLONGPRESS_DEBTIME)
		{
			APP_BTNMNGR_BTN_1_STATE = VERY_LONG_PRESS;
		}
		else if(APP_BTNMNGR_BTN_1_DCNTR >= APP_BTNMNGR_LONGPRESS_DEBTIME)
		{
			APP_BTNMNGR_BTN_1_STATE = LONG_PRESS;
		}
		else if(APP_BTNMNGR_BTN_1_DCNTR >= APP_BTNMNGR_SHORTPRESS_DEBTIME)
		{
			APP_BTNMNGR_BTN_1_STATE = SHORT_PRESS;
		}
		else
		{
			APP_BTNMNGR_BTN_1_STATE = NOT_PRESSED;
		}

		if(APP_BTNMNGR_BTN_1_DCNTR >= APP_BTNMNGR_VERYLONGPRESS_DEBTIME)
		{
			//Don't increment
		}
		else
		{
			APP_BTNMNGR_BTN_1_DCNTR++;
		}
	}
	else
	{
		APP_BTNMNGR_BTN_1_STATE = NOT_PRESSED;
		APP_BTNMNGR_BTN_1_DCNTR = 0;
	}
}

/************************************************************************/
/* Name: app_BtnMngr_DebBtn2                                            */
/* Description: TBD                     								*/
/* Parameters: None                                                     */
/* Return: None                                                         */
/************************************************************************/
static void app_BtnMngr_DebBtn2(void)
{
	//Check input
	if(0x00 == APP_BTNMNGR_READ_BTN_2)
	{//Button Pressed
		//Check debounce
		if(APP_BTNMNGR_BTN_2_DCNTR >= APP_BTNMNGR_VERYLONGPRESS_DEBTIME)
		{
			APP_BTNMNGR_BTN_2_STATE = VERY_LONG_PRESS;
		}
		else if(APP_BTNMNGR_BTN_2_DCNTR >= APP_BTNMNGR_LONGPRESS_DEBTIME)
		{
			APP_BTNMNGR_BTN_2_STATE = LONG_PRESS;
		}
		else if(APP_BTNMNGR_BTN_2_DCNTR >= APP_BTNMNGR_SHORTPRESS_DEBTIME)
		{
			APP_BTNMNGR_BTN_2_STATE = SHORT_PRESS;
		}
		else
		{
			APP_BTNMNGR_BTN_2_STATE = NOT_PRESSED;
		}

		if(APP_BTNMNGR_BTN_2_DCNTR >= APP_BTNMNGR_VERYLONGPRESS_DEBTIME)
		{
			//Don't increment
		}
		else
		{
			APP_BTNMNGR_BTN_2_DCNTR++;
		}
	}
	else
	{
		APP_BTNMNGR_BTN_2_STATE = NOT_PRESSED;
		APP_BTNMNGR_BTN_2_DCNTR = 0;
	}
}

/************************************************************************/
/* Name: app_BtnMngr_GetBtn1State                                       */
/* Description: TBD                     								*/
/* Parameters: None                                                     */
/* Return: None                                                         */
/************************************************************************/
T_BTN_State app_BtnMngr_GetBtn1State(void)
{
	return APP_BTNMNGR_BTN_1_STATE;
}

/************************************************************************/
/* Name: app_BtnMngr_GetBtn1State                                       */
/* Description: TBD                     								*/
/* Parameters: None                                                     */
/* Return: None                                                         */
/************************************************************************/
T_BTN_State app_BtnMngr_GetBtn2State(void)
{
	return APP_BTNMNGR_BTN_2_STATE;
}