/*
 * app_Addresser.h
 *
 * Created: 07/08/2019 12:54:13 a. m.
 *  Author: OmarSevilla
 */ 


#ifndef APP_ADDRESSER_H_
#define APP_ADDRESSER_H_

#define APP_ADDRESSER_UPDATETIME_MED	50u
#define APP_ADDRESSER_UPDATETIME_FAST	10u

extern unsigned char rub_ManualAddresserTick;

/* Public Functions */
extern void app_Addresser_Init(void);
extern void app_Addresser_SM(unsigned char lub_cmd);
extern void app_Addresser_ManualAddressing(void);
extern unsigned int app_DMXGetAddress(void);
extern void app_IncrementAddress(void);
extern void app_DecrementAddress(void);

#endif /* APP_ADDRESSER_H_ */