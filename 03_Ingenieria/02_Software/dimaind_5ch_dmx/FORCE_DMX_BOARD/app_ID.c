/*
* app_ID.c
*
* Created: 06/08/2019 11:40:04 p. m.
*  Author: OmarSevilla
*/

#include "app_ID.h"
#include "avr/boot.h"

/************************************************************************/
/* Name: app_ID_GetID                                      */
/* Description: TBD             										*/
/* Parameters: None                                                     */
/* Return: None                                                         */
/************************************************************************/
unsigned int app_ID_GetID(void)
{
	unsigned int luw_IDTemp;

	luw_IDTemp = boot_signature_byte_get(0x0E);
	luw_IDTemp |= boot_signature_byte_get(0x0F) << 8u;

	return (luw_IDTemp & 0x0FFF);
}