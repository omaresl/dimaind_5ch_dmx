/*
 * app_DMX.h
 *
 * Created: 27/07/2019 06:33:10 p. m.
 *  Author: OmarSevilla
 */ 


#ifndef APP_DMX_H_
#define APP_DMX_H_

#define DMX_N_CHANNELS			5u
#define DMX_MIN_ADDRESS			1u
#define DMX_MAX_ADDRESS			512u

/* Public Functions */
extern void app_DMX_Init(void);
extern void app_DMXRX_SM(void);
extern void app_DMXTXTest(void);
extern void app_DMXDataTask(void);
extern unsigned char app_DMXGetData(unsigned char lub_DataIndex);
extern void app_DMXResetSM(void);
extern unsigned int app_DMXGetAddress(void);
extern void app_DMXClearChannels(void);
extern void app_DMXSetREDChannels(void);
extern void app_DMXSetBLUEChannels(void);
extern void app_DMXSetNewAddress(unsigned int luw_Address);

#endif /* APP_DMX_H_ */